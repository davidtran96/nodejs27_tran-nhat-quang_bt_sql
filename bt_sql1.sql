# Tạo database
CREATE DATABASE bt_sql;
# sử dụng DATABASE bt_sql
USE bt_sql;
# Tạo bảng user
DROP TABLE user;

CREATE TABLE user(
    user_id INT NOT NULL AUTO_INCREMENT,
    full_name VARCHAR(100),
    email VARCHAR(50),
    password VARCHAR(50),
    PRIMARY KEY (user_id)
);
# Tạo bảng restaurant
CREATE TABLE restaurant(
    res_id int NOT NULL AUTO_INCREMENT,
    res_name VARCHAR(100),
    image VARCHAR(100),
    `desc` VARCHAR(100),
    PRIMARY KEY (res_id)
);
# Tại bảng rate_res
CREATE TABLE rate_res(
    user_id INT,
    res_id INT,
    amount INT,
    date_rate DATETIME,
    FOREIGN KEY (user_id) REFERENCES user(user_id),
    FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);
# Tạo bảng like_res
CREATE TABLE like_res(
    user_id INT,
    res_id INT,
    date_like DATETIME,
    FOREIGN KEY (user_id) REFERENCES user(user_id),
    FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);
# Tạo bảng foo_type
CREATE TABLE food_type(
    type_id INT NOT NULL AUTO_INCREMENT,
    type_name VARCHAR(100),
    PRIMARY KEY (type_id)
);

# Tạo bảng food
CREATE TABLE food(
    food_id INT NOT NULL AUTO_INCREMENT,
    food_name VARCHAR(80),
    image VARCHAR(100),
    price FLOAT,
    `desc` VARCHAR(100),
    type_id INT,
    PRIMARY KEY (food_id),
    FOREIGN KEY (type_id) REFERENCES food_type(type_id)
);
# Tạo bảng sub_food
CREATE TABLE sub_food(
    sub_id INT NOT NULL AUTO_INCREMENT,
    sub_name VARCHAR(100),
    sub_price FLOAT,
    food_id INT,
    PRIMARY KEY (sub_id),
    FOREIGN KEY (food_id) REFERENCES food(food_id)
);
# Tạo bảng order
CREATE TABLE `order`(
	user_id INT,
	food_id INT,
	amount INT,
	code VARCHAR(20),
	arr_sub_id VARCHAR(100),
	FOREIGN KEY (user_id) REFERENCES user(user_id),
	FOREIGN KEY (food_id) REFERENCES food(food_id)
);

#Thêm dữ liệu vào bảng
# Thêm dữ liệu user
INSERT INTO user(full_name, email, password)
VALUES ("aaa", "aaa@email.com","1234"),
       ("bbb","bbb@email.com", "2345"),
       ("ccc","ccc@email.com","3456"),
       ("ddd","ddd@email.com","gsbsf"),
       ("eee","eee@mail.com","vdfasd"),
       ("fff","fff@mail.com","fada"),
       ("ggg","ggg@mail.com","asvaw");

# Thêm dữ liệu vào restaurant
INSERT INTO restaurant(res_name, image, `desc`)
VALUES ("Store 1","image1.jpg","đây là cửa hàng 1"),
       ("Store 2","image2.jpg","đây là cửa hàng 2"),
       ("Sotre 3", "image3.jpg","đây là cửa hàng 3");
# Thêm dữ liệu vào rate_res
INSERT INTO rate_res(user_id, res_id, amount, date_rate)
VALUES (2,1,5,"2022-10-16"),
       (2,2,4,"2022-09-16"),
       (3,3,5,"2022-09-16"),
       (3,1,5,"2022-09-19"),
       (4,2,3,"2022-09-16"),
       (5,2,5,"2023-01-11"),
       (6,1,1,"2022-09-16"),
       (6,2,4,"2022-09-16"),
       (6,3,3,"2022-09-16");

# Thêm dữ liệu vào like_res
INSERT INTO like_res(user_id, res_id, date_like)
VALUES (2,1,"2022-11-19"),
	   (2,2,"2022-11-19"),
	   (4,3,"2022-11-15"),
	   (6,2,"2022-11-19"),
	   (3,3,"2022-11-19"),
	   (5,1,"2022-11-19"),
	   (7,1,"2022-11-19"),
	   (3,2,"2022-11-19"),
	   (2,3,"2022-11-19"),
	   (4,2,"2022-11-19");

# Thêm dữ liệu vào food_type
INSERT INTO food_type(type_name)
VALUES ("Khai vị"),
       ("Salad & Súp"),
       ("Món chính"),
       ("Tráng miệng"),
       ("Nước uống");


# Thêm dữ liệu vào food
INSERT INTO food(food_name, image, price, `desc`, type_id)
VALUES ("Chả giò cua", "img1.png",129000,"Deep-fried crab spring roll",1),
       ("Chả giò tôm", "img2.png",145000,"Deep-fried shrimp spring roll",1),
       ("Gỏi cuốn", "img3.png",109000,"fresh spring roll",2),
       ("Gỏi sứa Nha Trang", "img4.png",139000,"Nhatrang Jelly fish salad",2),
       ("Sườn non muối chiên", "img5.png",159000,"Deep-fried salted pork rib",3),
       ("Cơm gà Hải Nam", "img6.png",239000,"Hainanese chicken rice",3),
       ("Bò lúc lắc", "img7.png",185000,"shaking beef cubes",3),
       ("Sữa chua nếp cẩm", "img8.png",35000,"Yogurt Black Sticky Rice Pudding",4),
       ("Sữa chua tổ yến", "img9.png",45000,"Homemade Bird's Nest Yogurt",4),
       ("Rau má dừa", "img10.png",45000,"Centella With Coconut Juice",5),
       ("Nước me gừng", "img11.png",45000,"Tamarind Ginger & Roasted Peanut",5);

# Thêm dữ liệu vào sub_food
INSERT INTO sub_food(sub_name, sub_price, food_id)
VALUES ("Chả giò thêm",30000,1),
       ("Rau thêm",20000,1),
       ("Chả giò thêm",70000,2),
       ("Rau thêm",20000,2),
       ("Rau thêm",20000,3);

# Thêm dữ liệu vào order
INSERT INTO `order`(user_id, food_id, amount, code, arr_sub_id)
VALUES (2,1,2,"B01","1,2"),
       (3,2,2,"B02","1,2"),
       (3,4,2,"B02","1,2"),
       (3,10,2,"B02","1,2"),
       (4,5,1,"B03","1,2"),
       (5,1,5,"B04","2,3"),
       (5,5,5,"B04","2,4");

# Tìm 5 người đã like nhà hàng nhiều nhất
SELECT like_res.user_id, user.full_name, user.email, COUNT(res_id) AS 'Tổng Like'
FROM like_res, user
WHERE like_res.user_id = user.user_id
GROUP BY user_id
ORDER BY COUNT(res_id) DESC
LIMIT 5;

# Tỉm 2 nhà hàng có lượt like nhiều nhất
SELECT res_name, COUNT(like_res.res_id) AS 'Tổng Like'
FROM restaurant, like_res
WHERE restaurant.res_id = like_res.res_id
GROUP BY res_name
ORDER BY COUNT(like_res.res_id) DESC
LIMIT 2;

# Tìm người đặt hàng nhiều nhất
SELECT full_name,email, COUNT(`order`.user_id) AS 'Tổng order'
FROM user, `order`
WHERE user.user_id = `order`.user_id
GROUP BY user.user_id
ORDER BY COUNT(`order`.user_id) DESC
LIMIT 1;

# Tìm người dùng không hoạt động
SELECT user_id, full_name, email FROM user
WHERE user_id NOT IN (SELECT user_id FROM like_res) AND
      user_id NOT IN (SELECT user_id FROM `order`) AND
      user_id NOT IN (SELECT user_id FROM rate_res);

# Tính trung bình sub_food của một food
SELECT food.food_id, food_name, image, price, `desc`, AVG(sub_price)
FROM food, sub_food
WHERE food.food_id = sub_food.food_id
GROUP BY food.food_id


